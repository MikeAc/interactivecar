from  PyQt5.QtWidgets import QDialog, QScrollArea,QMessageBox,QLayout
from  PyQt5.QtWidgets import (QLineEdit,QPushButton,QLabel,QVBoxLayout,QHBoxLayout) 
from PyQt5.QtWidgets import (QFileDialog,QGroupBox)
from PyQt5.QtGui import QIcon
from dbManager import (db,query)
from PyQt5.QtCore import (QDir,Qt,QSize ,QFile,pyqtSignal,QDataStream,QByteArray)
from PyQt5.QtNetwork import QTcpSocket
from animatedwidget import Animated

from PyQt5.QtSql import (QSqlDatabase,QSqlQuery)
class mediaAdder(QDialog):

    """Docstring for mediaAdder. """
    addingfinished=pyqtSignal()
    def __init__(self,parent=None):
        """@todo: to be defined1. """
        super(QDialog,self).__init__(parent)
        self.mainlay=QVBoxLayout(self)
        self.scrollArea=QScrollArea(self)
        self.group=QGroupBox(self)
        self.layscroll=QVBoxLayout(self)
        self.layscroll.setAlignment(Qt.AlignTop)
        self.count=0 
        self.group.setTitle("Recursos");
        self.group.setLayout(self.layscroll)
        self.scrollArea.setWidget(self.group)
        self.scrollArea.setWidgetResizable(True)
        self.listgroup=[]
        
        
        
        self.pushs=(
        QPushButton(QIcon(QDir.currentPath()+"/plus.png"),"Imagen",self),
        QPushButton(QIcon(QDir.currentPath()+"/plus.png"),"Video",self),
        QPushButton(QIcon(QDir.currentPath()+"/minus.png"),"",self),
        QPushButton(QIcon(QDir.currentPath()+"/ok.png"),"", self),
        QPushButton(QIcon(QDir.currentPath()+"/door.png"),"", self)
        )
        self.lineName=QLineEdit(self)
        self.laysH=(QHBoxLayout(self),QHBoxLayout(self))
        
        self.laysH[0].addWidget(QLabel("Nombre: "))
        self.laysH[0].addWidget(self.lineName)
        for buts in self.pushs:
            self.laysH[1].addWidget(buts)
            buts.setIconSize(QSize(80,80))
        self.laysH[1].setAlignment(Qt.AlignRight)
        

        self.mainlay.addLayout(self.laysH[0])    
        self.mainlay.setAlignment(self.laysH[0],Qt.AlignTop)
        self.mainlay.addWidget(self.scrollArea)         
        self.mainlay.addLayout(self.laysH[1])    
        self.mainlay.setAlignment(self.laysH[1],Qt.AlignBottom)
        self.lineName.setPlaceholderText("EJ:Comer")
        for indx in range(0,2):
            self.pushs[indx].clicked.connect(self.addLines)
        self.pushs[2].clicked.connect(self.takelines) 
        self.pushs[3].clicked.connect(self.finishing)
        self.pushs[4].clicked.connect(self.inclose)
        self.setLayout(self.mainlay)
        self.mediatype="" 
        self.networkFilesize=0
        self.filetmpcont=0
        self.networkFileName="" 
        self.bytescounter=0
        self.networkFile=QFile()
        self.stream=QDataStream()
        self.dialogActivated=False
        self.transferingFile=False
        self.currlineEdit=None
    def infileRequest(self,fileF:list)->bool:
        if self.dialogActivated is False:
            return False
        if fileF[0]==self.mediatype:
            try:
                self.bytescounter=0
                self.networkFileName=fileF[1]
                self.networkFilesize=int(fileF[2])
                print("NetworkFileName:{}Filesize{}".format(self.networkFileName,self.networkFilesize)) 
                filepath=QDir.tempPath()+"/"+str("{}{}".format(self.filetmpcont,self.networkFileName))
                self.networkFile=QFile(filepath)
                if not self.networkFile.open(QFile.WriteOnly | QFile.Truncate):
                    print("error creating file")
                    return False
                
                self.currlineEdit.setText(filepath)
                self.stream=QDataStream(self.networkFile)
            except Exception as e:
                print(" error{}".format(str(e)))
                return False
            return True

        return  False

    def inNetworkFile(self,sock:QTcpSocket):
        if self.transferingFile is False:
            self.interactDialog.gif.start()
        self.transferingFile=True
        self.bytescounter=self.bytescounter+sock.bytesAvailable()
        print("bytes available {} Bytescountel={} File Size Sent= {}".format(sock.bytesAvailable(),self.bytescounter,self.networkFilesize))
        buff=sock.readAll()
        print("bufferSize={}".format(buff.size()))
        self.stream.writeRawData(buff.data())
      # self.networkFilesize=self.networkFilesize-buff.size()
    
    def endingFileTrans(self,sock:QTcpSocket):

        print("disconnected processing")
        if self.bytescounter==self.networkFilesize:
            self.networkFile.close()
            self.count=self.count+1
        else:
            self.networkFile.remove()
            self.networkFile.close()
        sock.deleteLater()
        self.transferingFile=False
        try:
            self.interactDialog.close()
        except:
            pass
        self.dialogActivated=False
        print("disconnected processed")
    

    def inclose(self):
        """@todo: Docstring for inclose.
        :returns: @todo

        """
        try:
            self.interactDialog.close()
        except:
            pass
        self.clear()
        self.hide()
    def addLines(self):
        """@todo: Docstring for addlines.
        :returns: @todo

        """
        group=QGroupBox(self)
        linemain=QLineEdit(self)
        group.setFixedHeight(100)
        currcount=self.count 
        if self.sender() is self.pushs[0]: 
            hbox=QHBoxLayout(self)
            box=QVBoxLayout(self)
            group.setTitle("Imagen")
            button=QPushButton(QIcon(QDir.currentPath()+"/phone.png"),"",self)
            button.clicked.connect(lambda: self.phoneSlots(linemain,"picture"))
            linemain.focusInEvent=lambda event : self.addlocalfile(linemain,"image_{}".format(currcount))
            hbox.addWidget(linemain)
            hbox.addWidget(button) 
            box.addLayout(hbox)
            

            hbox=QHBoxLayout(self)
            linesound=QLineEdit(self)
            linesound.focusInEvent= lambda e : self.addlocalfile(linesound,"sound_{}".format(currcount))
            button=QPushButton(QIcon(QDir.currentPath()+"/phone.png"),"",self)
            button.clicked.connect(lambda: self.phoneSlots(linesound,"audio"))
            hbox.addWidget(linesound)   
            hbox.addWidget(button) 
            box.addLayout(hbox)
            linemain.setPlaceholderText("Ruta imagen")
            linesound.setPlaceholderText("Ruta sonido")
            linemain.setReadOnly(True)
            linesound.setReadOnly(True)
            grouplist=("image",group,linemain,linesound)
        elif self.sender() is self.pushs[1]: 
            hbox=QHBoxLayout(self)
            box=QVBoxLayout(self)
            button=QPushButton(QIcon(QDir.currentPath()+"/phone.png"),"",self)
            button.setObjectName("video_{}".format(self.count))

            hbox.addWidget(linemain)
            hbox.addWidget(button)
            group.setTitle("Video")
            linemain.setPlaceholderText("Video")
            linemain.focusInEvent=lambda event : self.addlocalfile(linemain,"video_{}".format(currcount))
            button.clicked.connect(lambda: self.phoneSlots(linemain,"video"))
            linemain.setReadOnly(True)
            box.addLayout(hbox)
            grouplist=("video",group,linemain)
            
        self.count=self.count+1 
        group.setLayout(box)
        self.listgroup.append(grouplist)
        self.layscroll.addWidget(group) 

    def takelines(self):
        """@todo: Docstring for takelines.
        :returns: @todo

        """
        try:
            widgetlist=self.listgroup.pop() 
        except IndexError :
            QMessageBox.information(self,"Ups","Ya no hay mas elementos")
            self.count=0
            return
        
        #widget=QGroupBox()
        self.layscroll.removeWidget(widgetlist[1])
        widgetlist[1].deleteLater()
        self.count=self.count-1
    def phoneSlots(self,lineObject:QLineEdit ,namepos:str ):
        """@todo: Docstring for phoneSlots.

        :arg1: @todo
        :returns: @todo

        """
        #lineObject.setText(namepos) 
        print(namepos)
        
        if self.dialogActivated:
            return
        self.interactDialog=Animated( QDir.currentPath()+"/wait.gif", self)
        self.interactDialog.showMaximized()
        self.interactDialog.move(self.x()+300,self.y())
        self.dialogActivated=True
        self.interactDialog.gif.stop()
        self.mediatype=namepos
        self.currlineEdit=lineObject
        


    def addlocalfile(self,lineObject: QLineEdit,namepos:str ):
        """@todo: Docstring for addlocalfile.
        :returns: @todo

        """
        if self.dialogActivated:
            return
        strfilter=""
        if "image"  in namepos:
            strfilter="Image Files (*.png *.jpg *.jpeg *.gif )"
        elif "sound"  in namepos :
            strfilter="Sound Files (*.mp3 *.ogg *.m4a *.wma )"
        elif "video"  in namepos :
            strfilter="Video Files (*.mp4 *.mpeg *.avi *.mkv)"

        try:
            filename=QFileDialog.getOpenFileName(self,"Archivos","/home/pi",strfilter) 
            if not filename[0] :
                 return
            lineObject.setText(filename[0])
        except Exception:
            print(filename)
        
    def finishing(self):
        """@todo: Docstring for finishing.
        :returns: @todo

        """
        if  not self.lineName.text(): 
            QMessageBox.information(self,"Ups","Debe  ponerle nombre a la rutina.")
            return
 
        if len(self.listgroup) == 0 :
            QMessageBox.information(self,"Ups","Debe agregar recursos primero")
            return
        for touples in self.listgroup:
            for i in range(2, len(touples)):
                if not touples[i].text() : 
                    QMessageBox.information(self,"Ups","No puede dejar campos vacios")
                    return
        
        query.exec("select Name from Activities where Name='{}'".format(self.lineName.text()))
        if query.next() is True:
            QMessageBox.information(self,"Ups","El dato ya existe")
            return
        counter=0
        dirs=QDir(QDir.homePath()+"/.procarfiles")
        dirs.mkdir(self.lineName.text())
        path=dirs.absolutePath()+"/"+self.lineName.text()+"/"
        for touples in self.listgroup:
            sufix=touples[2].text().split('.')[-1] 
            QFile.copy(touples[2].text(),"{}{}a.{}".format(path,counter,sufix))

            if "image" in touples[0]:
                    #QFile.copy(touples[2].text(),"{}.
                sufix=touples[3].text().split('.')[-1] 
                QFile.copy(touples[3].text(),"{}{}b.{}".format(path,counter,sufix))
            counter=counter+1
         
        db.exec("insert into Activities (Name) values ('{}');".format(self.lineName.text()) )          
        self.addingfinished.emit()
    def clear(self):
        """@todo: Docstring for clear.
        :returns: @todo

        """

        self.transferingFile=False
        self.dialogActivated=False
        self.lineName.setText("")
        for touples in self.listgroup:
            self.layscroll.removeWidget(touples[1])
            touples[1].deleteLater()
        self.listgroup.clear()
        

    def mouseDoubleClickEvent(self,QMouseEvent):
        try:
            if not self.transferingFile:
                self.interactDialog.close()
                self.dialogActivated=False

        except:
            pass

