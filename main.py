#!/usr/bin/env python3

from PyQt5.QtWidgets  import QApplication 
from mainwindow import mainwindow
from PyQt5.QtCore import QDir
import sys 
app=QApplication(sys.argv)
savedir=QDir(QDir.homePath()+"/.procarfiles")
if savedir.exists() is False: 
    savedir.mkdir(QDir.homePath()+"/.procarfiles")
main=mainwindow()
main.show()
main.resize(800,480)
sys.exit(app.exec_())


