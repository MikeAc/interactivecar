from PyQt5.QtWidgets    import (QDialog,QWidget,QListWidget,QListWidgetItem)
from PyQt5.QtWidgets import QVBoxLayout,QHBoxLayout
from PyQt5.QtWidgets import QLayout,QMessageBox
from PyQt5.QtWidgets    import QStackedWidget
from multimediawidget import MultimediaWidget
from PyQt5.QtWidgets import QSizePolicy
from digitalclock import DigitalClock
from PyQt5.QtCore import Qt,QSize,QDir,QTimer
from PyQt5.QtWidgets    import QPushButton
from PyQt5.QtNetwork import QUdpSocket,QHostAddress,QNetworkInterface,QTcpServer,QTcpSocket
from PyQt5.QtCore   import QByteArray
from PyQt5.QtGui    import QIcon,QPixmap
from activitiesWdg import Activities
from schedule import Schedule
from dbManager import (db,initDatabase)
from MediaAdder import mediaAdder
from PyQt5.QtSql import QSqlDatabase
from omxplayer.player import OMXPlayer
import RPi.GPIO as GPIO
import logging
class mainwindow(QDialog):

    """Docstring for mainwindow. """

    def __init__(self,parent=None):
        """@todo: to be defined1. """
        super(QDialog,self).__init__(parent)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup([3,5,7,11],GPIO.OUT)

        GPIO.output([3,11],GPIO.LOW)
        GPIO.output([5,7],GPIO.LOW)
        

        initDatabase("cardb.db")
        print(db.isOpen()) 
        #crear layouts y wigets necesarios 
        
        self.mainlay=QVBoxLayout(self)
        self.clock=DigitalClock(self)
        self.pushbutton=QPushButton(self)
        self.multimedias=MultimediaWidget(self)
        self.createControlWidgets()
               
        self.pushbutton.setText("Salir")
        self.pushbutton.setMinimumSize(150,60)
        self.mainlay.setSizeConstraint(QVBoxLayout.SetMaximumSize)
        self.mainlay.addWidget(self.clock)
        self.mainlay.addWidget(self.controlWidget)  
        self.mainlay.addWidget(self.pushbutton)
        
        self.mainlay.setAlignment(self.clock,Qt.AlignRight)  
        self.mainlay.setAlignment(self.pushbutton,Qt.AlignRight)  
        self.setLayout(self.mainlay)
        self.pushbutton.clicked.connect(self.closewin)
        
        #create tcp sockets
        self.tcpfilesock=QTcpServer(self)
        self.tcpcmdsock=QTcpServer(self)
        self.tcpfilesock.newConnection.connect(self.processfile)
        self.tcpcmdsock.newConnection.connect(self.cmdnewtcpconnection)
        if ( self.tcpfilesock.listen(QHostAddress.AnyIPv4,6551) is False ) :
            QMessageBox.Critical(self,"Procar","No se pudo iniciar el servidor {}".format(self.tcpcmdsock))
            self.close()
            return
        if  self.tcpcmdsock.listen(QHostAddress.AnyIPv4,6552) is False :
            QMessageBox.Critical(self,"Procar","No se pudo iniciar el servidor {}".format(self.tcpcmdsock))
            self.close()
            return



        #create udpsockets
        self.serversock=QUdpSocket(self)
        self.serversockbrcast=QUdpSocket(self)
        self.serversock.bind(6551)
        self.serversockbrcast.bind(6552,QUdpSocket.ShareAddress)
        self.serversock.readyRead.connect(self.processingIns)
        self.serversockbrcast.readyRead.connect(self.broadcastResponse)
        self.serversock.connected.connect(self.connectionReceived) 
          
        #self.timer=QTimer(self)
        #self.timer.timeout.connect(self.timing)
        #self.timer.start(1000)
        self.timercont=0

    def connectionReceived(self):
        print("connection Received")
    
    def timing(self):
        ipdatagram=str("ok:mensaje {}".format(self.timercont))
        ipdatagram=bytearray(ipdatagram,encoding='ascii')
        self.serversockbrcast.writeDatagram(ipdatagram,QHostAddress(QHostAddress.Broadcast),6552)      
        self.timercont=self.timercont+1
     
    def createControlWidgets(self):
        optionVerticalLayout=QVBoxLayout(self)
        self.controlWidget=QWidget(self) 
        optionsLay=QHBoxLayout(self)
        self.optionlist= QListWidget(self)
        self.listActivities=Activities(self)
        self.schedules=Schedule(self)
        self.mediaAdd=mediaAdder(self)
        self.listActivities.fullfill()

        self.optionsStack=QStackedWidget(self)
        self.buttonplus=QPushButton(self)
        self.buttonplus.setIcon(QIcon(QDir.currentPath()+"/plus.png"))       
        self.buttonplus.setIconSize(QSize(40,40))
 
        optionVerticalLayout.addWidget(self.optionlist) 
        optionVerticalLayout.addWidget(self.buttonplus)
        optionsLay.addLayout(optionVerticalLayout)
        optionsLay.addWidget(self.optionsStack)
        
        
        self.optionsStack.addWidget(self.listActivities)
        self.optionsStack.addWidget(self.schedules)
        
        self.controlWidget.setLayout(optionsLay)
        self.optionlist.setViewMode(QListWidget.IconMode)
        self.optionlist.setSortingEnabled(False)
        self.optionlist.setMaximumWidth(self.width()/6)
        self.optionlist.itemClicked.connect(self.selectincOptions)
        item=QListWidgetItem(QIcon(QPixmap(QDir.currentPath()+"/highway.png")),"Rutas") 
        item.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsUserCheckable)
        item.setTextAlignment(Qt.AlignCenter)
        item.setSizeHint(QSize(100,100))
        self.optionlist.addItem(item)
        
        item=QListWidgetItem(QIcon(QPixmap(QDir.currentPath()+"/clock.png")),"Horarios") 
        item.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsUserCheckable)
        item.setTextAlignment(Qt.AlignCenter)
        item.setSizeHint(QSize(100,100))
        self.optionlist.addItem(item)
            
        self.buttonplus.clicked.connect(self.mediaAdd.showFullScreen)
        self.mediaAdd.addingfinished.connect(self.itemadded)
        self.listActivities.itemClicked.connect(self.openNewMultimedia)
        

    
    def cmdnewtcpconnection(self):
        tcpinsock=self.tcpcmdsock.nextPendingConnection()
        tcpinsock.disconnected.connect(tcpinsock.deleteLater)
        tcpinsock.readyRead.connect(self.processtcpcommands)
        print(" nueva conexion" )
    def processfile(self):
        tcpsock=self.tcpfilesock.nextPendingConnection()  
        tcpsock.disconnected.connect(lambda: self.mediaAdd.endingFileTrans(tcpsock))
        tcpsock.readyRead.connect(lambda:self.mediaAdd.inNetworkFile(tcpsock))
        print("conexion para archivo" )



    def processtcpcommands(self):
        tcpinsock=self.sender()
        buff=tcpinsock.readAll()
        buff=str(buff,encoding='ascii')
        print(buff)
        ans=self.processCommands(buff)
        tcpinsock.write(ans)
        tcpinsock.disconnectFromHost()
        

    def selectincOptions(self,QListWidgetItem ):
        self.optionsStack.setCurrentIndex(self.optionlist.currentRow())
     

    def broadcastResponse(self):
        while self.serversockbrcast.hasPendingDatagrams():
                try:
                    
                    datagram,host,port=self.serversockbrcast.readDatagram(self.serversockbrcast.pendingDatagramSize())
                    datagram=str(datagram,encoding='ascii')
                    print(datagram)
                    if "cmd" in datagram:
                        if "get_ip" in datagram:
                            for address in QNetworkInterface.allAddresses():
                                if address.protocol()== QUdpSocket.IPv4Protocol and address!= QHostAddress(QHostAddress.LocalHost):
                                    ipdatagram=str("ok:ip_request_response")
                                    ipdatagram=bytearray(ipdatagram,encoding='ascii')
                                    QTimer.singleShot(500,lambda:
                                    self.serversockbrcast.writeDatagram(ipdatagram,QHostAddress(QHostAddress.Broadcast),6552))

                            

                    
                except TypeError as e:
                    logging.exception("Failed"+str(e))

    def processingIns(self):
        while self.serversock.hasPendingDatagrams():
            datagram,host,port= self.serversock.readDatagram(self.serversock.pendingDatagramSize())
            try:
                datagram=str(datagram,encoding='ascii')
        
            except TypeError:
                pass

        print(datagram)
        if datagram == "ctrl":
            self.multimedias.next()

        if("cmd" in datagram):
            self.processCommands(datagram)
        self.movingCar(datagram)
        
   
    def processCommands(self,cmd:str)->tuple :
        tupl=()
        if "file"  in cmd:
            filespec=cmd.split(":")
            filespec.pop(0)
            if self.mediaAdd.infileRequest(filespec):
                responsemsg="ok:allowed"
            else:
                responsemsg="ok:forbidden" 

            responsemsg=bytearray(responsemsg,encoding='ascii')
            tupl=(responsemsg)
        return tupl
            
        

    def movingCar(self,move:str="" ):

        print(move)
        if(move == "up"):
            GPIO.output([3,7],GPIO.HIGH)
            print("upentro")
            GPIO.output([5,11],GPIO.LOW)
        elif move=="Dwn" :
             
            GPIO.output([5,11],GPIO.HIGH)
            GPIO.output([3,7],GPIO.LOW)
            
        elif move=="Lft" :
            GPIO.output([3,11],GPIO.HIGH)
            GPIO.output([5,7],GPIO.LOW)

        elif move=="Rht" :
            GPIO.output([5,7],GPIO.HIGH)
            GPIO.output([3,11],GPIO.LOW)

    def openNewMultimedia(self,item):
        self.multimedias.deleteLater()
        self.multimedias=MultimediaWidget(self)
        self.multimedias.initMultimedia(item.text())
        print("")     
    def closewin(self): 
        GPIO.cleanup()
        self.close()
    def itemadded(self):
        """@todo: Docstring for ititemadded.
        :returns: @todo

        """
        self.mediaAdd.inclose()
        self.listActivities.fullfill()

