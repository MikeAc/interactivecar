from PyQt5.QtWidgets import QListWidget
from dbManager import query
from PyQt5.QtSql import QSqlQuery
from PyQt5.QtWidgets import QListWidgetItem
from PyQt5.QtGui import QFont


class Activities(QListWidget):

    """Docstring for Activities. """

    def __init__(self,parent=None):
        """@todo: to be defined1. """
        super(QListWidget,self).__init__(parent) 
        self.font=QFont("DK Nouveau Crayon",56)



    def fullfill(self):
        self.clear()
        query.exec("select Name from Activities where 1")
        while query.next() is True:
            item=QListWidgetItem(self)
            item.setText(query.value(0))
            item.setFont(self.font)
            self.addItem(item)
            
             
