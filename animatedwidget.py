from PyQt5.QtWidgets import *
from PyQt5.QtGui import QMovie
from PyQt5.QtCore import QDir
from PyQt5.QtGui import QPainter
from PyQt5.QtGui import QPixmap
class Animated(QWidget):

    """Docstring for Animated. """

    def __init__(self,path,parent=None):
        """TODO: to be defined1. """
        super(QWidget,self).__init__(parent)
        self.gif=QMovie(path)
        self.gif.frameChanged.connect(self.repaint)
        self.gif.start()
        self.setMinimumSize(200,300)
        self.setSizePolicy(QSizePolicy.Maximum,QSizePolicy.Maximum)

    def paintEvent(self,QPaintEvent):
        painter=QPainter(self)
        pix=self.gif.currentPixmap()
        painter.drawPixmap(0,0,pix.scaled(self.size()))

    def resizeEvent(self,QResizeEvent):
        self.repaint()
