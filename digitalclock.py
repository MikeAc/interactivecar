from PyQt5.QtWidgets import QLCDNumber
from PyQt5.QtCore   import QTimer
from PyQt5.QtCore   import QTime



class DigitalClock(QLCDNumber):

    """Docstring for DigitalClock. """

    def __init__(self,parent=None):
        """@todo: to be defined1. """
        super(QLCDNumber,self).__init__(parent)
        self.setSegmentStyle(QLCDNumber.Flat)
        self.timer=QTimer(self)
        self.timer.timeout.connect(self.showtTime)
        self.timer.start(1000)
        self.showtTime()
        self.setMaximumSize(300, 120)
    
    def showtTime(self):
        time=QTime.currentTime()
        text=time.toString("hh:mm")
        if (time.second() % 2) == 0 :
            text.replace(":"," ")
        self.display(text)
