from PyQt5.QtWidgets import  QWidget,QDialog,QPushButton
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import QStackedWidget
from animatedwidget import Animated
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtMultimedia import QMediaPlayer,QMediaContent
from PyQt5.QtCore import QIODevice,QUrl,QDir,Qt,QRect,QPoint,QTimer
from omxplayer.player import OMXPlayer
class MultimediaWidget(QDialog):


    """Docstring for MultimediaWidget. """

    def __init__(self,parent=None):
        """@todo: to be defined1. """
        super(QDialog,self).__init__(parent)
        self.stack=QStackedWidget(self)
        #self.videoW=QVideoWidget(self)  
        self.pushbutton=QPushButton(self)
        self.pushbutton.setText("Salir")
        self.pushbutton.setMinimumSize(150,60)
        self.emptyVideoWidg=QWidget(self)

        self.pushbutton.clicked.connect(self.close)
        self.listFiles=[]
        self.widgetlist=[]
        self.lay=QVBoxLayout(self)
        self.lay.addWidget(self.stack)
        self.lay.addWidget(self.pushbutton)
        self.setLayout(self.lay)
        self.mediaplayer=QMediaPlayer(self)
        self.lay.setAlignment(self.pushbutton,Qt.AlignRight)  
        self.cont=0
       
        try:
            self.player=OMXPlayer("")
            self.player.set_aspect_mode("stretch")
        except:
            print("player on constructor")
            pass


    def initMultimedia(self,name: str):
        #self.resize(800,480)
        self.showFullScreen()
    
        dirm=QDir(QDir.homePath()+"/.procarfiles/"+name)  
        bufflist= dirm.entryList(QDir.Files);
        flag=False
        for i in range(0, len(bufflist)):
            list2=[]
            for filename in bufflist:
                if "{}".format(i) in filename:
                    list2.append(QDir.homePath()+"/.procarfiles/"+name+"/"+filename)
                    flag=True
            if flag :
                self.listFiles.append(list2)
            else:
                break
            flag=False
        self.initWidg()
        
    
    def adaptVideo(self):
        #parent=self.parentWidget()
        rect=QRect(self.mapToGlobal(self.stack.geometry().topLeft()),self.mapToGlobal(self.stack.geometry().bottomRight()))
        print("TopLeft:{} BottomRight: {} ".format(rect.topLeft(),rect.bottomRight()))        
        try:
            self.player.set_video_pos(rect.topLeft().x(), rect.topLeft().y(), rect.bottomRight().x(), rect.bottomRight().y()) 
        except:
            pass



    def resizeEvent(self,QResizeEvent):
        print("resizeEvent: " )
        #self.adaptVideo()


    def moveEvent(self,QMoveEvent):
        print("hola" )
        #self.adaptVideo()
       
    def  initWidg(self):
        for files in self.listFiles:
            if len(files)>1:
                self.stack.addWidget(Animated(files[0]))
                print("imgs:{}".format(files))
            else:
                self.stack.addWidget(QWidget(self))
                print("video:{}".format(files))
        try:
            if len(self.listFiles[0])>1:
                self.player.load(self.listFiles[0][1])    
            else:
                self.player.load(self.listFiles[0][0])
                QTimer.singleShot(1000,Qt.CoarseTimer,self.adaptVideo)
                #self.player.play()
       
        except:
            if len(self.listFiles[0])>1:
                self.player=OMXPlayer(self.listFiles[0][1])
                print("create first sound" )
                #self.player.play()
            else:

                self.player=OMXPlayer(self.listFiles[0][0])
                QTimer.singleShot(1000,Qt.CoarseTimer,self.adaptVideo)
                #self.player.play()

            print("not loaded.. creating object")
        self.stack.setCurrentIndex(0)
   
            
               
    
    def next(self):
        self.cont+=1
        self.stack.setCurrentIndex(self.cont)
        try:
            self.player.stop()
        except:
            pass
        try:
            if len(self.listFiles[self.cont])>1:
                
                self.player.load(self.listFiles[self.cont][1])
            else:   
                self.player.load(self.listFiles[self.cont][0])
                self.adaptVideo()

        except IndexError :
            self.onClose()
            print(self.cont)
        except Exception as e:
                print(str(e))

   
    def closeEvent(self,QCloseEvent):
        self.player.quit()
    def onClose(self):
        self.player.quit()
        self.close()
